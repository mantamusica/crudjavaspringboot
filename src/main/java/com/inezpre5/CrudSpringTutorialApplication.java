package com.inezpre5;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudSpringTutorialApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudSpringTutorialApplication.class, args);
	}
}