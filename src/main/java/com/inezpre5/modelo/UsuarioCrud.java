package com.inezpre5.modelo;

import org.springframework.data.repository.CrudRepository;

public interface UsuarioCrud extends CrudRepository<Usuario, Long>{

}
