package com.inezpre5.controlador;
 
import javax.validation.Valid;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.Gson;
import com.inezpre5.modelo.Usuario;
import com.inezpre5.modelo.UsuarioCrud;
 
@Controller
@RequestMapping("/crud")
public class ControladorCrud {
 
	//Hacemos uso de la interfaz del objeto
    @Autowired
    private UsuarioCrud uc;
 
    //Listar
    @RequestMapping(value="", method = RequestMethod.GET)
    public String inicio(){
        return "crud/index";
    }
    //Listar
    @RequestMapping(value="lista", method = RequestMethod.GET)
    public String listaUsuarios(ModelMap mp){
        mp.put("usuarios", uc.findAll());
        return "crud/lista";
    }
    //Botón de Crear
    @RequestMapping(value="/nuevo", method=RequestMethod.GET)
    public String nuevo(ModelMap mp){
        mp.put("usuario", new Usuario());
        return "crud/nuevo";
    }
    //Acción de Crear
    @RequestMapping(value="/crear", method=RequestMethod.POST)
    public String crear(@Valid Usuario usuario,
            BindingResult bindingResult, ModelMap mp){
        if(bindingResult.hasErrors()){
            return "/crud/nuevo";
        }else{
            uc.save(usuario);
            mp.put("usuario", usuario);
            return "crud/creado";
        }
    }
    //Acción de creado
    @RequestMapping(value="/creado", method = RequestMethod.POST)
    public String creado(@RequestParam("usuario") Usuario usuario){
        return "/crud/creado";
    }
    //Acción de borrar y volver a listar
    @RequestMapping(value="/borrar/{id}", method=RequestMethod.GET)
    public String borrar(@PathVariable("id") long id, ModelMap mp){
        uc.delete(uc.findById(id).orElse(null));
        mp.put("usuarios", uc.findAll());
        return "crud/lista";
    }
    //Botón de editar
    @RequestMapping(value="/editar/{id}", method=RequestMethod.GET)
    public String editar(@PathVariable("id") long id, ModelMap mp){
        mp.put("usuario", uc.findById(id).orElse(null));
        return "crud/editar";
    }
    //Acción de editar y volvemos a listar
    @RequestMapping(value="/actualizar", method=RequestMethod.POST)
    public String actualizar(@Valid Usuario usuario, BindingResult bindingResult, ModelMap mp){
    	
    	Gson gson = new Gson();

    	System.out.println(gson.toJson(mp));
    	
        if(bindingResult.hasErrors()){
            mp.put("usuarios", uc.findAll());
            
        return "crud/lista";
        }
        
        Usuario user = uc.findById(usuario.getId()).orElse(null);

        user.setNombre(usuario.getNombre());
        user.setPassword(usuario.getPassword());
        user.setEmail(usuario.getEmail());

        uc.save(user);
        mp.put("usuario", user);
        return "crud/actualizado";
    }
}